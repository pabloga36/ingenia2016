
#include "stdafx.h"
#include "osg\LineSegment"
#include "osgUtil\IntersectVisitor"	
#include "objetoMovil.h"
#include "FuncionesAyuda.h"
extern osg::Image *imagen_heightmap;
objetoMovil::objetoMovil(double a, double b, double esc, osg::Group * grupo, osg::Node * geometria,double ost,double offset_ang)
	:objeto(a, b, esc, grupo, geometria,ost, offset_ang)
{
	vx = 0.0;
	vy = 0.0;
	w = 0.0;
	w2 = 0.0;
	ax = 0;
	// usar chrono
	// t= ::GetCurrentTime();
}

void objetoMovil::setVX(double v) {
	vx = v;
}

void objetoMovil::setVY(double v) {
	vy = v;
}

void objetoMovil::setW(double v) {
	w = v;
}

void objetoMovil::setW2(double v) {
	w2 = v;
}
void objetoMovil::setAx(double a) {
	ax = a;
}

bool objetoMovil::movimiento(osgViewer::Viewer* viewer) {

		//Declaracion de varaiables necesarias
		osg::Matrix mtras, mrotz, mesc;
		double x_ant = x;
		double y_ant = y;
		double z_ant = z;
		double intervalo = inc_tiempo_milli(t);
		bool movimiento = true;
		vx = vx + ax*intervalo;
		if (vx > VELOCIDAD * 1.25) {
			vx = VELOCIDAD * 1.25;
		}
		else if (vx < 0 && ax != 0) {
			vx = 0;
		}
		double x_post = x +vx*intervalo*cos(alpha) - vy*intervalo*sin(alpha);
		double y_post = y + vx*intervalo*sin(alpha) + vy*intervalo*cos(alpha);
		double z_post = obtenerAltura(x_post, y_post, MALLA, SEPARACION, H_MAX, imagen_heightmap)+ offset;
		alpha = alpha + w *intervalo;
		theta = theta + w2*intervalo;
		if (alpha > 2*PI_) {
			alpha -= 2 * PI_;
		}else if (theta > -PI_/4) {
			theta = -PI_/4;
		}
		if (theta <-3*PI_/4) {
			theta = -3 * PI_ / 4;
		}
		else if (alpha < 0) {
			alpha += 2 * PI_;
		}
		
		mrotz = mrotz.rotate(alpha+ost_ang, osg::Vec3d(0, 0, 1));
		mesc = mesc.scale(escala, escala, escala);
		if ((abs(x_post)<(SEPARACION*MALLA-1)/2.0) && (abs(y_post)<(SEPARACION*MALLA-1)/2.0) && (vx != 0 || vy != 0) && (z_post - z_ant) / sqrt((x_post - x_ant)*(x_post - x_ant) + (y_post - y_ant)*(y_post - y_ant)) < 1) {
			
			//osgUtil::LineSegmentIntersector personLocationSegment(osg::Vec3(x_ant, y_ant, z_ant), osg::Vec3(x_post, y_post, z_post));
			//osgUtil::LineSegmentIntersector::Intersection interseccion =personLocationSegment.getFirstIntersection();
			osg::ref_ptr<osgUtil::LineSegmentIntersector> intersector =
				new osgUtil::LineSegmentIntersector(osg::Vec3(x_ant, y_ant, z_ant+0.3), osg::Vec3(2*x_post-x_ant, 2*y_post-y_ant, z_post+0.3));
			osgUtil::IntersectionVisitor iv(intersector.get());
			viewer->getCamera()->accept(iv);

			// First intersection: 
			osgUtil::LineSegmentIntersector::Intersection firstHit =
				intersector->getFirstIntersection();
			
			
			//Asignar los puntos iniciales del rayo
			//Devolver las nuevas coordenadas resultado de la interseccion
			if (intersector->getIntersections().size()>2 || (intersector->getIntersections().size() > 0 && intersector->getIntersections().size() <= 2 && firstHit.nodePath[4]->getName().find("Sam_legs")))
			{
				
					mtras = mtras.translate(x, y, z);
					movimiento = false;
				
			}
			else {

				x = x_post;
				y = y_post;
				z = z_post;
				mtras = mtras.translate(x_post, y_post, z_post);
				

			}

		}
		else {
			mtras = mtras.translate(x, y, z);
			movimiento = false;
		}

		mt->setMatrix(mrotz*mesc*mtras);
		t = std::chrono::system_clock::now();
		return movimiento;
	}

void objetoMovil::dirigir(double xf, double yf) {
	osg::Vec2 v = osg::Vec2(xf-x,yf-y);
	v.normalize();
	double ang = acos(v.x());
	if (v.y() < 0) {
		ang = 2*PI_-ang;
	}
	if (ang < alpha) {
		ang += 2 * PI_;
	}
	ang -= alpha;
	if (ang > PI_) {
		ang -= 2 * PI_;
	}
	w = ang / 250.0;
	ax = 1.0 / 1000000.0 - 10 / 1000000 * abs(ang);
	return;
}
objetoMovil::~objetoMovil()
{
}
