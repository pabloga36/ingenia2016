
#pragma once

class objeto
{
public:
	objeto(double a, double b, double esc, osg::Group * grupo, osg::Node * geometria,double ost, double offset_ang);
	double X();
	double Y();
	double Z();
	double Alpha_rad();
	double Theta_rad();
	double AlphaG();
	double ThetaG();
	~objeto();
protected:
	double x;
	double y;
	double z;
	double alpha;
	double theta;
	double escala;
	double offset;
	double ost_ang;
	osg::MatrixTransform *mt;
};