#pragma once

#include "objeto.h"

class objetoMovil :
	public objeto
{
public:
	objetoMovil(double a, double b, double esc, osg::Group * grupo, osg::Node * geometria,double ost,double offset_ang);
	bool objetoMovil::movimiento(osgViewer::Viewer* viewer);
	void objetoMovil::setVX(double v);
	void objetoMovil::setVY(double v);
	void objetoMovil::setW(double v);
	void objetoMovil::setW2(double v);
	void objetoMovil::setAx(double a);
	void objetoMovil::dirigir(double x, double y);
	~objetoMovil();
protected:
	float vx;
	float vy;
	float w;
	float w2;
	double ax;
	std::chrono::time_point<std::chrono::system_clock>  t;
};

