#include <winsock2.h>
#include "stdafx.h"


#pragma once
struct Texto {
	osg::Switch* s;
	osgText::Text* t;
};


class texto_HUD
{
public:
	static struct Texto texto_HUD::add(float width_rel, float height_rel, const char* texto);
	static void texto_HUD::desactivar(osg::Switch*s);
	static void texto_HUD::activar(osg::Switch*s);
	static void texto_HUD::modify(struct Texto t, const char * c);
};

