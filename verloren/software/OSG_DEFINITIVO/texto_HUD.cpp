
#include "stdafx.h"
#include "texto_HUD.h"


extern osg::Camera *camara3;
extern osg::ref_ptr<osgText::Font> font;
extern int width, height;
struct Texto texto_HUD::add(float width_rel,float height_rel, const char* texto)

{
	struct Texto t;
		osg::Geode* geode_texto_intro = new osg::Geode();
		osgText::Text* nombre = new osgText::Text;
		geode_texto_intro->addDrawable(nombre);
		//nombre->setFont(font);
		nombre->setPosition(osg::Vec3(-width / 2.0 + width_rel*width / 100.0, -height / 2.0 + height_rel*height / 100.0, 0));
		nombre->setFontResolution(60, 60);
		nombre->setCharacterSizeMode(osgText::Text::SCREEN_COORDS);
		nombre->setCharacterSize(15);
		nombre->setAlignment(osgText::Text::LEFT_BOTTOM);
		nombre->setDataVariance(osg::Object::DYNAMIC);
		geode_texto_intro->setDataVariance(osg::Object::DYNAMIC);
		geode_texto_intro->setCullingActive(false);
		nombre->setText(texto);
		
		osg::Switch* s = new osg::Switch;
		s->addChild(geode_texto_intro);
		camara3->addChild(s);
		t.s = s;
		t.t =nombre;
		return t;
}
void texto_HUD::modify(struct Texto t, const char* c) {
	t.t->setText(c);
	//printf("%s", c);
}

		void texto_HUD::desactivar(osg::Switch*s)
		{
			s->setAllChildrenOff();

		}
		void texto_HUD::activar(osg::Switch*s)
		{
			s->setAllChildrenOn();
		}




