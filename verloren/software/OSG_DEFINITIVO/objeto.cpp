
#include "stdafx.h"

#include "objeto.h"
#include "FuncionesAyuda.h"
extern osg::Image *imagen_heightmap;
objeto::objeto(double a, double b, double esc, osg::Group * grupo, osg::Node * geometria, double ost,double offset_ang)
	:x(a), y(b), offset(ost), ost_ang(offset_ang)
{
	osg::Matrix mtras, mrotz;
	osg::Matrixd mesc;
	mt = new osg::MatrixTransform;
	alpha = 0;
	theta = -PI_/2;
	mrotz = mrotz.rotate(alpha+offset_ang, osg::Vec3d(0, 0, 1));
	escala = esc ;
	mesc = mesc.scale(esc, esc, esc);
	z = obtenerAltura(a, b, MALLA, SEPARACION, H_MAX, imagen_heightmap) + offset;
	mtras = mtras.translate(a, b, z);
	mt->setMatrix(mrotz*mesc*mtras);
	mt->setNodeMask(0x3);
	mt->addChild(geometria);
	grupo->addChild(mt);	
	
}



double objeto::X() {
	return x;
};
double objeto::Y() {
	return y;
};
double objeto::Z() {
	return z;
};
double objeto::Alpha_rad() {
	return alpha;
};
double objeto::Theta_rad() {
	return theta;
};
double objeto::AlphaG() {
	return alpha*180.0 / PI_  ;
};
double objeto::ThetaG() {
	return theta*180.0 / PI_;
};

objeto::~objeto()
{
}