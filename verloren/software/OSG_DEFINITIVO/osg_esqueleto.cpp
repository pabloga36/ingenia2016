﻿
#include "stdafx.h"

#include "FuncionesAyuda.h"
#include "config_file.h"
#include "objeto.h"
#include "objetoMovil.h"
#include "texto_HUD.h"
#include "Image_HUD.h"
#include "inventario.h"

//Variables globales


int width, height;
double posx, posy;
int H = 0.5;
int L = 0.5;
osgViewer::Viewer viewer;
std::chrono::time_point<std::chrono::system_clock> t = std::chrono::system_clock::now();
struct Imagen hambre;
osg::Image* imagen_heightmap = osgDB::readImageFile(HEIGHTMAP);
osg::Geometry* imagen = new osg::Geometry;
osg::Camera* camera_texto = new osg::Camera;
osg::Camera* camara2 = new osg::Camera;
osg::Camera* camara3 = new osg::Camera;
osg::ref_ptr<osgText::Font> font = osgText::readRefFontFile("fonts/cambria.ttc");
double vx, vy, radio=5.0,wrot,w2,x_cam,y_cam,z_cam;
std::string recurso = "";
bool mostrando=false;
bool raton = false;
bool coger = 0;
int n_mision = 1;
struct Texto mision;
bool construir=false;
std::chrono::time_point<std::chrono::system_clock> t_mensaje = std::chrono::system_clock::now();


class SimulacionKeyboardEvent : public osgGA::GUIEventHandler
{
protected:
public:
	SimulacionKeyboardEvent::SimulacionKeyboardEvent()
	{
	};
	struct control_teclado
	{
		double x;
		double y;

	} tecla_pulsada;

	virtual bool SimulacionKeyboardEvent::handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter&)
	{
		int tecla = ea.getKey();
		int evento = ea.getEventType();
		int shift = ea.getModKeyMask();
		int button = ea.getButton();
		double x = ea.getX();
		double y = ea.getY();
		switch (evento)
		{
		case(osgGA::GUIEventAdapter::MOVE):
		{
			raton = true;
			wrot = SENSIBILIDAD*(width / 2 -x)/(radio*10000);
			w2 = SENSIBILIDAD*(height / 2 -y) / (radio*10000);
			if (wrot > PI_ / 1000.0) {
				wrot = PI_ / 1000.0;
			}
			else if (wrot < -PI_ / 1000.0) {
				wrot = -PI_ / 1000.0;
			}
			if (w2 > PI_ / 1000.0) {
				w2 = PI_ / 1000.0;
			}
			else if (w2 < -PI_ / 1000.0) {
				w2 = -PI_ / 1000.0;
			}
			viewer.requestWarpPointer(width/2,height/2);
			
		}
		
		case(osgGA::GUIEventAdapter::KEYDOWN):	//Evento pulsacion tecla
		{
			switch (tecla) {
				case'a':
					vx = VELOCIDAD;
					break;
				case'd':
					vx = -VELOCIDAD;
					break;
				case'w':
					vy = -VELOCIDAD;
					break;
				case's':
					vy = VELOCIDAD;
					break;
				case'j':
					wrot = PI_/1500.0;
					raton = false;
					break;
				case'l':
					wrot = -PI_ / 1500.0;
					raton = false;
					break;
				case'i':
					w2 = PI_ / 4000.0;
					raton = false;
					break;
				case'k':
					w2 = -PI_ / 4000.0;
					raton = false;
					break;
				case'e':
					coger=true;
					
					break;
				case 'g':
					if (H > 0)
					{
						H -= 0.5;
						t = std::chrono::system_clock::now();
						Image_HUD::modify(0.5, 7.5, 20 - H, 12, hambre.g);

					}
					break;
				case 'c':
					if (n_mision == 2) {
						construir = true;
						t_mensaje = std::chrono::system_clock::now();
					}
					break;
				default:
					break;
			}
			break;
		}
		//case (osgGA::GUIEventAdapter::PUSH):
		case(osgGA::GUIEventAdapter::KEYUP):	// Se deja de pulsar una tecla
		{
			switch (tecla) {
			case'a':
				vx = 0;
				break;
			case'd':
				vx = 0;
				break;
			case'w':
				vy = 0;
				break;
			case's':
				vy = 0;
				break;
			case'j':
				wrot = 0;
				break;
			case'l':
				wrot = 0;
				break;
			case'i':
				w2 = 0;
				break;
			case'k':
				w2 = 0;
				break;
			case'r':
				mostrando = !mostrando;
				break;
			default:
				break;
			}
			break;
		}
		default:
			break;
		}
		return 0;
		
	}
	};

	
	int main(int, char**)
	{
		
		viewer.setUpViewInWindow(50, 50, 800, 600, 0);
		unsigned int w, h;
		osg::GraphicsContext::WindowingSystemInterface* wsi =
			osg::GraphicsContext::getWindowingSystemInterface();
		if (!wsi)
		{
				return 0;
		}
		wsi->getScreenResolution(osg::GraphicsContext::ScreenIdentifier(0),
			w, h);
		width = w;
		height = h;

		osgViewer::Viewer::Windows windows;
		viewer.getWindows(windows);
		(*windows.begin())->useCursor(0);

		osg::Group* grupo = new osg::Group;
		//osg::Group * grupo_hijo = new osg::Group;
		viewer.setSceneData(grupo);
		viewer.addEventHandler(new SimulacionKeyboardEvent());
		osg::Geode* malla = CreaMalla(MALLA, SEPARACION);											// Crea la malla del suelo
		AddTexture(malla, HEIGHTMAP, 0);		// Añade la textura a la malla
		AddTexture(malla, "..\\Data\\Texturas\\hm_normal.png", 1);		// Añade la textura a la malla
		AddTexture(malla, "..\\Data\\Texturas\\hm_txt.png", 2);		// Añade la textura a la malla
		AddTexture(malla, "..\\Data\\Texturas\\orangesand.png", 3);		// Añade la textura a la malla
		AddTexture(malla, "..\\Data\\Texturas\\longGrass.png", 4);		// Añade la textura a la malla
		AddTexture(malla, "..\\Data\\Texturas\\packDirt.png", 5);		// Añade la textura a la malla
		AddTexture(malla, "..\\Data\\Texturas\\ocean_texture.png", 7);
		AddShader(malla, "..\\Data\\heightmap_sombras.vert", "..\\Data\\heightmap_sombras.frag");
		malla->setCullingActive(false);
		//grupo_hijo->addChild(malla);															//Añade malla al grupo
		//grupo->addChild(grupo_hijo);																				
		
		//----------------------------- Luces ----------------------------------
		/*
		osg::Light* myLight = new osg::Light;
		myLight->setLightNum(0);
		myLight->setPosition(osg::Vec4(500.0, 500.0, 1000.0, 0));				// Posicion foco
		myLight->setAmbient(osg::Vec4(0.0f, 0.0f, 0.8f, 1.0f));				// Color luz ambiente
		myLight->setDiffuse(osg::Vec4(0.8f, 0.8f, 0.8f, 1.0f));				// Color luz difusa
		//myLight->setDirection(osg::Vec3(-1, -1, -2));					// Direccion     

		osg::ref_ptr<osg::LightSource> lightS = new osg::LightSource;
		lightS->setLight(myLight);
		lightS->setLocalStateSetModes(osg::StateAttribute::ON);*/

		osg::ref_ptr<osg::LightSource> source = new osg::LightSource;
		source->getLight()->setPosition(osg::Vec4(500.0, 500.0, 1000.0,0.0));
		source->getLight()->setAmbient(osg::Vec4(0.8, 0.8, 0.8, 1.0));
		source->getLight()->setDiffuse(osg::Vec4(0.6, 0.6, 0.6, 1.0));
		source->getLight()->setDirection(osg::Vec3(-1, -1, -2));
		source->getLight()->setLightNum(0);
		source->setLocalStateSetModes(osg::StateAttribute::ON);

		osg::Group * grupo_luz = new osg::Group;
		grupo_luz->addChild(source);
		grupo->addChild(grupo_luz);

		//------------------------------ Sombras ------------------------------------

		osgShadow::ShadowedScene* sc = new  osgShadow::ShadowedScene;
		osg::ref_ptr<osgShadow::ShadowMap> sm = new osgShadow::ShadowMap;
		sm->setLight(source.get());
		sm->setTextureSize(osg::Vec2s(1024, 1024));
		sm->setTextureUnit(1);
		const int ReceivesShadowTraversalMask = 0x1;
		const int CastsShadowTraversalMask = 0x2;
		const int RecAndCast = ReceivesShadowTraversalMask | CastsShadowTraversalMask;
		sc->setShadowTechnique(sm.get());
		sc->setReceivesShadowTraversalMask(ReceivesShadowTraversalMask);
		sc->setCastsShadowTraversalMask(CastsShadowTraversalMask);
		sc->setReceivesShadowTraversalMask(ReceivesShadowTraversalMask);
		sc->setCastsShadowTraversalMask(CastsShadowTraversalMask);


		grupo->addChild(sc);
		viewer.setSceneData(grupo);



		//--------------------------- Camara_texto ------------------------------- 
		//osg::Camera* camera_texto = new osg::Camera;
		camera_texto->setProjectionMatrix(osg::Matrix::ortho2D(-width / 2.0, width / 2.0, -height / 2.0, height / 2.0));
		camera_texto->setReferenceFrame(osg::Transform::ABSOLUTE_RF);
		camera_texto->setViewMatrix(osg::Matrix::identity());
		camera_texto->setClearMask(GL_DEPTH_BUFFER_BIT);
		camera_texto->setRenderOrder(osg::Camera::POST_RENDER,0);
		grupo->addChild(camera_texto);
		camara2->setProjectionMatrix(osg::Matrix::ortho2D(-width / 2.0, width / 2.0, -height / 2.0, height / 2.0));
		camara2->setReferenceFrame(osg::Transform::ABSOLUTE_RF);
		camara2->setViewMatrix(osg::Matrix::identity());
		camara2->setClearMask(GL_DEPTH_BUFFER_BIT);
		camara2->setRenderOrder(osg::Camera::POST_RENDER, 1);
		grupo->addChild(camara2);
		camara3->setProjectionMatrix(osg::Matrix::ortho2D(-width / 2.0, width / 2.0, -height / 2.0, height / 2.0));
		camara3->setReferenceFrame(osg::Transform::ABSOLUTE_RF);
		camara3->setViewMatrix(osg::Matrix::identity());
		camara3->setClearMask(GL_DEPTH_BUFFER_BIT);
		camara3->setRenderOrder(osg::Camera::POST_RENDER, 2);
		grupo->addChild(camara3);

		

		//----------------------------- Textos e Imagenes ----------------------------
		//Textos
		struct Texto titulo = texto_HUD::add(0, 97, "Lost Paradise v_alfa_0.5");
		struct Texto vida_texto = texto_HUD::add(7, 5.2, "VIDA");
		struct Texto hambre_texto = texto_HUD::add(6, 12.2, "HAMBRE");
		mision = texto_HUD::add(70, 90, "Mision: Recolectar recursos");
		struct Texto recoger = texto_HUD::add(45, 97, "");
		struct Texto construccion = texto_HUD::add(45, 80, "Necesitas 20 de Madera\n y 20 de piedra");
		texto_HUD::desactivar(construccion.s);

		// Barra de hambre

		hambre = Image_HUD::add(0.5, 7.5, 20, 12, 0, "..//Data//Texturas//HUD//color-amarillo.png");
		// Barra de vida
		struct Imagen vida = Image_HUD::add(0.5, 0.5, 20, 5, 0, "..//Data//Texturas//HUD//barra_de_vida.png");
		struct Imagen vida_rojo = Image_HUD::add(0.5, 0.5, 20, 5, 0, "..//Data//Texturas//HUD//Heightmap_rgb.png");

		struct Imagen fondo = Image_HUD::add(0.5, 0.5, 22, 14, 0, "..//Data//Texturas//HUD//gris.jpg");
		// Mapa
		//struct Imagen flecha = Image_HUD::add(88, 5, 88 + SEPARACION, 5 + SEPARACION, 0, "..//Data//Texturas//HUD//Heightmap_rgb.png");
		//struct Imagen mapa = Image_HUD::add(82, 0.5, 99.5, 19, 0, "..//Data//Texturas//Nuevoheightmap\\nuevoHeightmap2_c.png");
		//-------------Geometrias------------------
			


		osg::MatrixTransform * mt = NULL;
		osg::Matrixd mtras, mrotz, mrotx, mroty, mesc, mtotal;
		std::map <std::string, osg::Node *> geometrias;
		std::map <std::string, std::string> recursos;
		int i = 0;
		osg::Billboard* BB = NULL;
		float escala = 1.0, xpos = 0.0, ypos = 0.0, zpos = 0.0;
		geometrias["personaje"] = osgDB::readNodeFile(".\\modelos\\Jennifer\\Jennifer.obj");
		geometrias["dinosaurio"] = osgDB::readNodeFile(".\\modelos\\OSGB\\dino.OSGB");
		geometrias["lancha"] = osgDB::readNodeFile(".\\modelos\\OSGB\\zodiac.OSGB");
		geometrias["casa"] = osgDB::readNodeFile(".\\modelos\\OSGB\\casa.OSGB");
		geometrias["cielo"]= osgDB::readNodeFile(".\\modelos\\OSGB\\cielo_desp.OSGB"); 

		osg::MatrixTransform* mt2 = new osg::MatrixTransform;

		mt2->addChild(geometrias["cielo"]);
		grupo->addChild(mt2);
		mtras = mtras.translate(0, 0, -5);
		mrotz = mrotz.rotate(PI_ / 4, osg::Vec3d(0, 0, 1));
		mesc = mesc.scale(10.0, 10.0, 10.0);
		mtotal = mesc*mrotz*mtras;
		mt2->setMatrix(mtotal);

		using namespace rapidxml;
		config_file config("./layout.xml");
		std::string fichero;
		xml_document<> doc;    // character type defaults to char
		doc.parse<0>(config.buffer);    // 0 means default parse flags
		auto node_datos = doc.first_node();
		for (xml_node<> *node = node_datos->first_node(); node != NULL; node = node->next_sibling())
		{
			mt = new osg::MatrixTransform;
			for (xml_node<> *nodo = node->first_node(); nodo != NULL; nodo = nodo->next_sibling())
			{
				if (strcmp(nodo->name(), "Fichero") == 0) {
					fichero = nodo->value();
					if (geometrias.find(fichero) == geometrias.end())
					{
						geometrias[fichero] = osgDB::readNodeFile(nodo->value());
					}
					mt->addChild(geometrias[fichero]);
				}
				if (strcmp(nodo->name(), "Recurso") == 0) {
					if (recursos.find(fichero) == recursos.end())
					{
						fichero=fichero.substr(fichero.find_last_of("\\")+1);
						fichero=fichero.substr(0, fichero.find_last_of("."));
						//printf("%s", fichero.c_str());
						recursos[fichero] = nodo->value();
					}
				}

				if (strcmp(nodo->name(), "Escala") == 0) {
					escala = atof(nodo->value());

				}

				if (strcmp(nodo->name(), "Offset") == 0) {
					zpos = atof(nodo->value());
				}
				if (strcmp(nodo->name(), "Xpos") == 0) {
					xpos = atof(nodo->value())/1.3;

				}

				if (strcmp(nodo->name(), "Ypos") == 0) {
					ypos = atof(nodo->value())/1.3;

				}

			}
			zpos += obtenerAltura(xpos, ypos, MALLA, SEPARACION, H_MAX, imagen_heightmap);
			if(obtenerAltura(xpos, ypos, MALLA, SEPARACION, H_MAX, imagen_heightmap)>4 && abs(xpos)<(SEPARACION*MALLA-1)/2.0 && abs(ypos)<(SEPARACION*MALLA-1)/2.0){
				mesc = mesc.scale(escala, escala, escala);
				mtras = mtras.translate(xpos, ypos, zpos);
				mtotal = mesc*mtras;
				mt->setMatrix(mtotal);
				mt->setNodeMask(0x3);
				sc->addChild(mt);
			}
		}
		
		//BB = new osg::Billboard;
		//BB->setMode(osg::Billboard::AXIAL_ROT);
		//BB->setAxis(osg::Vec3(0, 0, 1));
		//BB->setNormal(osg::Vec3(0, -1, 0));
		//BB->addDrawable(geometrias["vrl_serg.3DS"]->asGeode()->getDrawable(0));
		//mt->addChild(BB);
		double offset = -0.025;
		objetoMovil personaje(0, 0,0.35, sc, geometrias["personaje"],offset,0);
		objetoMovil dinosaurio(-50, -50, 0.03, sc, geometrias["dinosaurio"],-0.5,PI_);
		//objeto esfera(0, 0, 100, sc, geometrias["cielo"], -100, 0);
		std::chrono::time_point<std::chrono::system_clock>  t_dino;
		t_dino = std::chrono::system_clock::now();
		objeto lancha(53, -85, 0.0005, sc, geometrias["lancha"], offset,PI_/2);
		sc->addChild(malla);
		//Iniciacion colision
		osg::ref_ptr<osgUtil::LineSegmentIntersector> intersector =
			new osgUtil::LineSegmentIntersector(osg::Vec3(0, 0, 0), osg::Vec3(1, 1, 1));
		osgUtil::IntersectionVisitor iv(intersector.get());
		viewer.getCamera()->accept(iv);
		osgUtil::LineSegmentIntersector::Intersection firstHit =
			intersector->getFirstIntersection();
		inventario inv(3, 4);
		osg::Vec2 v,v2;
		double angulo = 0;
		viewer.getCamera()->setCullingMode(viewer.getCamera()->getCullingMode() & ~osg::Camera::VIEW_FRUSTUM_CULLING);

		osgUtil::Optimizer optimizer;
		optimizer.optimize(grupo);
		double x_dino, y_dino;
		x_dino = rand() % 201 - 100;
		y_dino = rand() % 201 - 100;
		bool dino_no_atascado=true;
		bool encontrado = false;
		viewer.realize();



		while (!viewer.done()) {
			if (n_mision < 4) {
				//printf("%f--%f\n", personaje.X(), personaje.Y());
				//Activar o desactivar inventario
				if (mostrando) {
					inv.mostrar();
				}
				else {
					inv.desactivar();
				}
				//movimiento personaje
				personaje.setVX(vx);
				personaje.setVY(vy);
				personaje.setW(wrot);
				personaje.setW2(w2);
				if (raton) {
					wrot = 0;
					w2 = 0;
				}
				personaje.movimiento(&viewer);
				x_cam = personaje.X() + radio* cos(personaje.Alpha_rad() - PI_ / 2)*sin(personaje.Theta_rad());
				y_cam = personaje.Y() + radio* sin(personaje.Alpha_rad() - PI_ / 2)*sin(personaje.Theta_rad());
				z_cam = personaje.Z() + radio*cos(personaje.Theta_rad()) + 1.0;
				if (z_cam < obtenerAltura(x_cam, y_cam, MALLA, SEPARACION, H_MAX, imagen_heightmap) + 0.2) {
					z_cam = obtenerAltura(x_cam, y_cam, MALLA, SEPARACION, H_MAX, imagen_heightmap) + 0.2;
				}
				viewer.getCamera()->setViewMatrixAsLookAt(osg::Vec3d(x_cam, y_cam, z_cam), osg::Vec3d(personaje.X(), personaje.Y(), personaje.Z() + 1.0), osg::Vec3d(0, 0, 1));
				//viewer.getCamera()->setViewMatrixAsLookAt(osg::Vec3d(0, 0, 100), osg::Vec3d(personaje.X(),personaje.Y(), 0), osg::Vec3d(1, 0, 0));
				// First intersection:
				for (angulo = -10 * PI_ / 180.0; (angulo <= 10 * PI_ / 180.0); angulo += 2 * PI_ / 180.0) {
					intersector->setStart(osg::Vec3d(personaje.X(), personaje.Y(), personaje.Z() + 0.3));
					intersector->setEnd((osg::Vec3d(personaje.X() - 1.5*cos(personaje.Alpha_rad() + PI_ / 2 + angulo), personaje.Y() - 1.5*sin(personaje.Alpha_rad() + PI_ / 2 + angulo), personaje.Z() + 0.3)));
					iv.setIntersector(intersector.get());
					viewer.getCamera()->accept(iv);
					firstHit = intersector->getFirstIntersection();

					//----------------- recoger recursos --------------------------

					for (i = 0; (i < firstHit.nodePath.size()); i++) {
						//printf("%s", firstHit.nodePath[i]->getName().c_str());
						if (recursos.find(firstHit.nodePath[i]->getName()) != recursos.end()) {
							recurso = recursos[firstHit.nodePath[i]->getName()];
							//printf("%s", recurso.c_str());
							texto_HUD::modify(recoger, recurso.c_str());
							if (coger) {

								inv.añadir(recurso, 1, 20);
								if (n_mision = 1) {
									n_mision = 2;
									texto_HUD::modify(mision, "Mision: Construye una casa \n(Pulsa C)");
								}
							}
							encontrado = true;
							break;
						}
						if (encontrado) {
							break;
						}
					}

				}
				if (!encontrado) {
					texto_HUD::modify(recoger, "");
				}
				coger = false;
				encontrado = false;
				intersector->reset();
				if (inc_tiempo_sec(t) >= 5 && H < 20)
				{
					Image_HUD::modify(0.5, 7.5, 20 - H, 12, hambre.g);
					H = H++;
					t = std::chrono::system_clock::now();
					if (H <= 4 && L > 0) {
						L = L--;
						Image_HUD::modify(0.5, 0.5, 20 - L, 5, vida.g);
					}
				}
				if (inc_tiempo_sec(t) >= 5 && H >= 19.5 && L < 20)

				{
					L = L++;
					Image_HUD::modify(0.5, 0.5, 20 - L, 5, vida.g);

					t = std::chrono::system_clock::now();
				}
				//Construccion
				if (construir) {
					if (inv.buscar("Piedra") >= 20 && inv.buscar("Madera") >= 20) {
						objeto casa(personaje.X() - 5 * cos(personaje.Alpha_rad() + PI_ / 2), personaje.Y() - 5 * sin(personaje.Alpha_rad() + PI_ / 2), 0.05, sc, geometrias["casa"], -1, 0);
						//inv.quitar("Piedra", 20);
						//inv.quitar("Madera", 20);
						construir = false;
						texto_HUD::desactivar(construccion.s);
						texto_HUD::modify(mision, "encuentra la forma\n de escapar");


						n_mision++;
					}
					else {
						texto_HUD::modify(construccion, "No tiene los recursos necesarios\nNecesita 20 de piedra y 20 de madera");
						texto_HUD::activar(construccion.s);

						construir = false;
					}

				}
				if (n_mision == 2 && inc_tiempo_sec(t_mensaje) > 5) {
					texto_HUD::desactivar(construccion.s);
				}
				//Persecucion
				v = osg::Vec2(personaje.X() - dinosaurio.X(), personaje.Y() - dinosaurio.Y());
				if (v.length() < 20 && dino_no_atascado) {
					dinosaurio.dirigir(personaje.X(), personaje.Y());
				}
				else if (!dino_no_atascado) {
					dinosaurio.setVX(VELOCIDAD / 2);
					dinosaurio.setW(PI_ / 2000.0);
					dinosaurio.setAx(0);

				}
				else {
					v = osg::Vec2(x_dino - dinosaurio.X(), y_dino - dinosaurio.Y());
					if (v.length() < 0.5 || inc_tiempo_sec(t_dino) > 60) {
						x_dino = rand() % 201 - 100;
						y_dino = rand() % 201 - 100;
						dinosaurio.dirigir(x_dino, y_dino);
						t_dino = std::chrono::system_clock::now();

					}
					else {
						dinosaurio.dirigir(x_dino, y_dino);
						dinosaurio.setAx(0);
						dinosaurio.setVX(VELOCIDAD / 5);
					}
				}

				dino_no_atascado = dinosaurio.movimiento(&viewer);
				//Huida
				v2 = osg::Vec2(personaje.X() - lancha.X(), personaje.Y() - lancha.Y());
				if (n_mision == 3 && v2.length() < 1) {
					n_mision = 4;
					texto_HUD::modify(construccion, "¡Has logrado escapar!");
					texto_HUD::activar(construccion.s);
				}
				v2 = osg::Vec2(personaje.X() - dinosaurio.X(), personaje.Y() - dinosaurio.Y());
				if (L == 20 || v2.length() < 2) {
					n_mision = 4;
					texto_HUD::modify(construccion, "No has sobrevivido");
					texto_HUD::activar(construccion.s);
				}

				viewer.getWindows(windows);
				(*windows.begin())->useCursor(0);
			}
			viewer.frame();

		

		}
		return 0;

	}

