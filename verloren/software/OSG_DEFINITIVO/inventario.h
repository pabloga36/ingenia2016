#pragma once
#include "stdafx.h"
#include "Image_HUD.h"
#include "texto_HUD.h"
using namespace std;

struct hueco {
	string nombre;
	int cantidad;
	int cantidad_max;
	struct Imagen marco;
	struct Imagen obj;
	struct Texto t;
};


class inventario
{
public:
	inventario(int filas, int columnas);
	string a�adir(string objeto, int n_objeto, int n_objetomax);
	string quitar(string objeto, int n_objeto);
	void mostrar();
	void desactivar();
	int buscar(string objeto);
	~inventario();
protected:
	int n_filas;
	int n_columnas;
	struct hueco* casillas;
};
