#include "stdafx.h"
#include "inventario.h"
#include "Image_HUD.h"
#include "texto_HUD.h"

extern int width, height;

inventario::inventario(int filas, int columnas)
	:n_filas(filas), n_columnas(columnas)
{
	int i;
	int j;
	float xinicial = 10;
	float yinicial = 90;
	float xvert1, yvert1, yvert3, xvert3;
	float salto_ancho = (80 - 40) / n_columnas;
	float salto_alto = (40 - 80) / n_filas;
	casillas = new struct hueco[filas*columnas];
	for (i = 0; i < filas; i++) {
		for (j = 0; j < columnas; j++) {
			xvert1 = xinicial + j*salto_ancho;
			yvert1 = yinicial + i*salto_alto;
			xvert3 = xvert1 + salto_ancho;
			yvert3 = yvert1 + salto_alto;
			(casillas + i*columnas + j)->nombre = "";
			(casillas + i*columnas + j)->cantidad = 0;
			(casillas + i*columnas + j)->cantidad_max = 0;
			(casillas + i*columnas + j)->marco = Image_HUD::add(xvert1, yvert1, xvert3, yvert3, 0, ".\\modelos\\marco.png");
			Image_HUD::desactivar((casillas + i*columnas + j)->marco);
		}
	}
}

int inventario::buscar(string objeto)
{
	int i;
	int j;
	for (i = 0; i < n_filas; i++) {
		for (j = 0; j < n_columnas; j++) {
			if ((casillas + i*n_columnas + j)->nombre == objeto) {
				return (casillas + i*n_columnas + j)->cantidad;
			}
		}
	}
	return 0;
}

string inventario::a�adir(string objeto, int n_objeto, int n_objetomax)
{
	float xinicial = 10;
	float yinicial = 90;
	float xvert1, yvert1, yvert3, xvert3;
	float xi1, xi3, yi1, yi3;
	float salto_ancho = (80 - 40) / n_columnas;
	float salto_alto = (40 - 80) / n_filas;
	string cadena, informacion;
	int i;
	int j;
	int f = -1;
	int c = -1;
	for (i = 0; i < n_filas && f == -1; i++) {
		for (j = 0; j < n_columnas && c == -1; j++) {
			if ((casillas + i*n_columnas + j)->cantidad_max == 0 || (casillas + i*n_columnas + j)->nombre == objeto) {
				f = i;
				c = j;
			}
		}
	}
	if (f == -1 && c == -1) {
		return "No queda espacio en el inventario";
	}
	else if ((casillas + f*n_columnas + c)->cantidad_max == 0) {
		xvert1 = xinicial + f*salto_ancho;
		yvert1 = yinicial + c*salto_alto;
		xvert3 = xvert1 + salto_ancho;
		yvert3 = yvert1 + salto_alto;
		xi1 = xvert1 + 0.1*salto_ancho;
		yi1 = yvert1 + 0.1*salto_alto;
		xi3 = xvert1 + 0.90*salto_ancho;
		yi3 = yvert1 + 0.9*salto_alto;

		(casillas + f*n_columnas + c)->nombre = objeto;
		cadena = ".\\modelos\\" + objeto + ".jpeg";
		//printf("%s", cadena.c_str());
		(casillas + f*n_columnas + c)->obj = Image_HUD::add(xi1, yi1, xi3, yi3, 1, cadena.c_str());
		Image_HUD::desactivar((casillas + f*n_columnas + c)->obj);
		(casillas + f*n_columnas + c)->t = texto_HUD::add(xi3 - 0.19*salto_ancho, yi3 - 0.045*salto_alto, to_string(n_objeto).c_str());

		texto_HUD::desactivar((casillas + f*n_columnas + c)->t.s);
		(casillas + f*n_columnas + c)->cantidad = n_objeto;
		(casillas + f*n_columnas + c)->cantidad_max = n_objetomax;

		/*xt1 = xvert1 + 0.20*salto_ancho;
		yt1 = yvert3 + 0.30*salto_alto;
		informacion = (casillas + i*n_columnas + j)->nombre + " " + to_string((casillas + i*n_columnas + j)->cantidad);
		texto_HUD::add(xt1, yt1, informacion.c_str());*/
	}
	else {
		if ((casillas + f*n_columnas + c)->cantidad + n_objeto > (casillas + f*n_columnas + c)->cantidad_max) {
			(casillas + f*n_columnas + c)->cantidad = (casillas + f*n_columnas + c)->cantidad_max;
			cadena = to_string((casillas + f*n_columnas + c)->cantidad);
			texto_HUD::modify((casillas + f*n_columnas + c)->t, cadena.c_str());
			return "No puedes llevar m�s " + objeto;
		}
		else {
			(casillas + f*n_columnas + c)->cantidad += n_objeto;
			cadena = to_string((casillas + f*n_columnas + c)->cantidad);
			//printf("%s----n_objeto=%f\n", cadena.c_str(), n_objeto);
			texto_HUD::modify((casillas + f*n_columnas + c)->t, cadena.c_str());
			return "Se a�adi� " + objeto + " al inventario";
		}
	}
}

string inventario::quitar(string objeto, int n_objeto)
{

	int i;
	int j;
	int f = -1;
	int c = -1;
	for (i = 0; i < n_filas && f == -1; i++) {
		for (j = 0; j < n_columnas && c == -1; j++) {
			if ((casillas + i*n_columnas + j)->nombre == objeto) {
				f = i;
				c = j;
			}
		}
	}
	if (f == -1 && c == -1) {
		return "No queda " + objeto;
	}
	else {
		if ((casillas + f*n_columnas + c)->cantidad - n_objeto <= 0) {
			n_objeto = (casillas + f*n_columnas + c)->cantidad;
			for (i = f + 1; i < n_filas; i++) {
				for (j = c + 1; j < n_columnas; j++) {
					*(casillas + (i - 1)*n_columnas + j - 1) = *(casillas + i*n_columnas + j);
				}
			}
			(casillas + (n_filas - 1)*n_columnas + n_columnas)->nombre = "";
			(casillas + (n_filas - 1)*n_columnas + n_columnas)->cantidad = 0;
			(casillas + (n_filas - 1)*n_columnas + n_columnas)->cantidad_max = 0;
			//Image_HUD::quitar((casillas + (n_filas - 1)*n_columnas + n_columnas)->obj);
			//texto_HUD::modify((casillas + (n_filas - 1)*n_columnas + n_columnas)->t,"");
			return "Eliminado " + n_objeto + objeto;
		}
		else {
			(casillas + f*n_columnas + c)->cantidad -= n_objeto;
			return "Eliminado " + n_objeto + objeto;
		}
	}
	return "Fallo";
}

void inventario::mostrar()
{
	int i, j;

	for (i = 0; i < n_filas; i++) {
		for (j = 0; j < n_columnas; j++) {
			Image_HUD::activar((casillas + i*n_columnas + j)->marco);
			if ((casillas + i*n_columnas + j)->nombre != "") {
				Image_HUD::activar((casillas + i*n_columnas + j)->obj);
				texto_HUD::activar((casillas + i*n_columnas + j)->t.s);
			}
		}
	}

	/*for (i = 0; i < n_filas; i++) {
	for (j = 0; j < n_columnas; j++) {
	xvert1 = xinicial + j*salto_ancho;
	yvert1 = yinicial + i*salto_alto;
	xvert3 = xvert1 + salto_ancho;
	yvert3 = yvert1 + salto_alto;

	if ((casillas + i*n_columnas + j)->nombre != "") {
	xi1 = xvert1 + 0.30*salto_ancho;
	yi1 = yvert3 + 0.80*salto_alto;
	xi3 = xvert1 + 0.70*salto_ancho;
	yi3 = yvert3 + 0.40*salto_alto;

	cadena = ".\\modelos\\" + (casillas + i*n_columnas + j)->nombre + ".png";
	struct Imagen utensilio = Image_HUD::add(xvert1, yvert1, xvert3, yvert3,0, cadena.c_str());

	xt1 = xvert1 + 0.20*salto_ancho;
	yt1 = yvert3 + 0.30*salto_alto;
	cadena = (casillas + i*n_columnas + j)->nombre+ " "+to_string( (casillas + i*n_columnas + j)->cantidad);
	texto_HUD::add(xt1, yt1, cadena.c_str());
	}
	}
	}*/

}

void inventario::desactivar() {
	int i;
	int j;

	for (i = 0; i < n_filas; i++) {
		for (j = 0; j < n_columnas; j++) {
			if ((casillas + i*n_columnas + j)->nombre != "") {
				Image_HUD::desactivar((casillas + i*n_columnas + j)->obj);
				texto_HUD::desactivar((casillas + i*n_columnas + j)->t.s);
			}
			Image_HUD::desactivar((casillas + i*n_columnas + j)->marco);
		}
	}
}

/*float x = width / 2.0;
float y = height / 2.0;
float xinicial = 10;
float yinicial = 90;
float xvert1, yvert1, yvert3, xvert3;
float xi1, xi3, yi1, yi3;
float xt1, yt1;
float salto_ancho = (80 - 20) / n_columnas;
float salto_alto = (40 - 60) / n_filas;
Image_HUD::desactivar(obj);
int i;
int j;
for (i = 0; i < n_filas; i++) {
for (j = 0; j < n_columnas; j++) {
xvert1 = xinicial + j*salto_ancho;
yvert1 = yinicial + i*salto_alto;
xvert3 = xvert1 + salto_ancho;
yvert3 = yvert1 + salto_alto;
if ((casillas + i*n_columnas + j)->nombre == objeto) {
Image_HUD::desactivar(obj);
}
}
}*/


inventario::~inventario()
{
}

