
#include "stdafx.h"
#include "Image_HUD.h"


extern osg::Camera *camera_texto;
extern osg::Camera* camara2;
extern int width, height;

struct Imagen Image_HUD::add(float xvertice1, float yvertice1, float xvertice3, float yvertice3, float z, const char* archivo)
{
	struct Imagen imagen;
	imagen.g = new osg::Geometry;
	
	// a�adir los cuatro vertices 
	osg::Vec3Array* pVerts = new osg::Vec3Array;
	pVerts->push_back(osg::Vec3(-width/2.0 + xvertice1*width / 100, -height / 2.0 + yvertice3*height/100, 0));//4
	pVerts->push_back(osg::Vec3(-width/2.0 + xvertice1*width/ 100, -height / 2.0 + yvertice1*height / 100, 0));//1
	pVerts->push_back(osg::Vec3(-width/ 2.0 + xvertice3*width / 100, -height / 2.0 + yvertice1*height / 100, 0));//2
	pVerts->push_back(osg::Vec3(-width / 2.0 + xvertice3*width / 100, -height / 2.0 + yvertice3*height/100, 0));//3
	imagen.g->setVertexArray(pVerts);

	// create a primitive set 
	osg::DrawElementsUInt* pPrimitiveSet = new osg::DrawElementsUInt(osg::PrimitiveSet::QUADS, 0);
	pPrimitiveSet->push_back(3);
	pPrimitiveSet->push_back(2);
	pPrimitiveSet->push_back(1);
	pPrimitiveSet->push_back(0);
	imagen.g->addPrimitiveSet(pPrimitiveSet);

	// create an arraw for texture coordinates 
	osg::Vec2Array* pTexCoords = new osg::Vec2Array(4);
	(*pTexCoords)[0].set(0.0f, 0.0f);
	(*pTexCoords)[1].set(1.0f, 0.0f);
	(*pTexCoords)[2].set(1.0f, 1.0f);
	(*pTexCoords)[3].set(0.0f, 1.0f);
	imagen.g->setTexCoordArray(0, pTexCoords);

	// create geometry node that will contain all our drawables 
	osg::Geode* pGeode = new osg::Geode;
	osg::StateSet* pStateSet = pGeode->getOrCreateStateSet();
	pStateSet->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
	pGeode->addDrawable(imagen.g);

	// load and set texture attribute and mode 
	osg::Texture2D* pTex = new osg::Texture2D;

	osg::Image* pImage = osgDB::readImageFile(archivo);

	pTex->setImage(pImage);
	pStateSet->setTextureAttributeAndModes(0, pTex, osg::StateAttribute::ON);
	imagen.g->setStateSet(pStateSet);

	imagen.s = new osg::Switch;
	imagen.s->addChild(imagen.g);

	if (z==0) {
		camera_texto->addChild(imagen.s);
	}
	else {
		camara2->addChild(imagen.s);
	}

	return imagen;


}

void Image_HUD::modify(float xvertice1, float yvertice1, float xvertice3, float yvertice3, osg::Geometry* imagen)
{
	osg::Vec3Array* vertices_mod = new osg::Vec3Array;
	vertices_mod->push_back(osg::Vec3(-width / 2.0 + xvertice1*width / 100, -height / 2.0 + yvertice3*height / 100, 0));//4
	vertices_mod->push_back(osg::Vec3(-width / 2.0 + xvertice1*width / 100, -height / 2.0 + yvertice1*height / 100, 0));//1
	vertices_mod->push_back(osg::Vec3(-width / 2.0 + xvertice3*width / 100, -height / 2.0 + yvertice1*height / 100, 0));//2
	vertices_mod->push_back(osg::Vec3(-width / 2.0 + xvertice3*width / 100, -height / 2.0 + yvertice3*height / 100, 0));//3
	imagen->setVertexArray(vertices_mod);

}

void Image_HUD::desactivar(struct Imagen i)
{
	i.s->setAllChildrenOff();

}
void Image_HUD::activar(struct Imagen i)
{
	i.s->setAllChildrenOn();

}

void Image_HUD::quitar(struct Imagen i)
{
	i.s->removeChildren(0,i.s->getNumChildren());
}
