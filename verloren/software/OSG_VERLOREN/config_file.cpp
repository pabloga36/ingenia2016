
#include "stdafx.h"
#include "config_file.h"
#include <direct.h>

config_file::config_file(const char *filename)
{
	char workingDirectory[256];
	buffer = NULL;
	FILE * pFile;
	long lSize;
	
	size_t result;

	_getcwd(workingDirectory, 256);

	pFile = fopen(filename, "rb");  //_CRT_SECURE_NO_WARNINGS
	if (pFile == NULL) { 
		fputs("File error, config file not found in", stderr); 
		fputs(workingDirectory, stderr);
		exit(1); 
	}

	// obtener tama�o del archivo
	fseek(pFile, 0, SEEK_END);
	lSize = ftell(pFile);
	rewind(pFile);

	buffer = (char*)malloc(sizeof(char)*lSize + 1);
	if (buffer == NULL) { fputs("Memory error", stderr); exit(2); }
	buffer[lSize] = 0;

	result = fread(buffer, 1, lSize, pFile);
	if (result != lSize) { fputs("Reading error", stderr); exit(3); }

	fclose(pFile);
}


config_file::~config_file()
{
	free(buffer);
}
