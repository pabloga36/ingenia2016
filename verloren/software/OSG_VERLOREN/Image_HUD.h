#include <winsock2.h>
#include "stdafx.h"


#pragma once
struct Imagen {
	osg::Switch *s;
	osg::Geometry *g;
};
class Image_HUD
{
public:
	static struct Imagen Image_HUD::add(float xvertice1, float yvertice1, float xvertice3, float yvertice3, float z, const char* texto);
	static void Image_HUD::modify(float xvertice1, float yvertice1, float xvertice3, float yvertice3, osg::Geometry* imagen);
	static void Image_HUD::desactivar(struct Imagen i);
	static void Image_HUD::activar(struct Imagen i);
	static void Image_HUD::quitar(struct Imagen i);
};

