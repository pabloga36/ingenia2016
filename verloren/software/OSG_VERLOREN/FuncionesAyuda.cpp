#include "stdafx.h"		
#include "FuncionesAyuda.h"	

float obtenerAltura(float posX, float posY, int num_puntos,double separacion_puntos,double altura_total, osg::Image* imagen_heightmap)
{
	/*double x_min = int(-num_puntos / 2) * separacion_puntos;
	double y_min = int(-num_puntos / 2) * separacion_puntos;
	
	double tamMalla = num_puntos*separacion_puntos; 
	
	double xGeom = (posX-x_min)/(tamMalla-1);
	double yGeom = (posY-y_min)/(tamMalla-1);

	double posZ = imagen_heightmap->getColor(osg::Vec2(xGeom, yGeom)).r()*altura_total;
	return posZ;*/
	double x_min = int(-num_puntos / 2) * separacion_puntos;
	double y_min = int(-num_puntos / 2) * separacion_puntos;

	double tamMalla = num_puntos*separacion_puntos;

	double xGeom = (posX - x_min);// / (tamMalla - 1);
	double yGeom = (posY - y_min);// / (tamMalla - 1);

	double i = (int(posX / separacion_puntos)*separacion_puntos - x_min);
	double j = (int(posY / separacion_puntos)*separacion_puntos - y_min);

	double pasoX = posX > 0 ? separacion_puntos : -separacion_puntos; //si x <0 cuento hacia izquierda
	double pasoY = posY > 0 ? separacion_puntos : -separacion_puntos; //si y <0 cuento hacia abajo

	osg::Vec3 a, b, c, vn;

	a = osg::Vec3(i, j, imagen_heightmap->getColor(osg::Vec2(i, j) / (tamMalla - 1)).r()*altura_total);
	b = osg::Vec3(i + pasoX, j + pasoY, imagen_heightmap->getColor(osg::Vec2(i + pasoX, j + pasoY) / (tamMalla - 1)).r()*altura_total);
	double dis1, dis2;
	dis1 = (xGeom - a.x())*(xGeom - a.x()) + (yGeom- b.y())*(yGeom - b.y());
	dis2 = (xGeom - b.x())*(xGeom - b.x()) + (yGeom - a.y())*(yGeom - a.y());
	if (dis1<= dis2) {
		c = osg::Vec3(a.x(), b.y(), imagen_heightmap->getColor(osg::Vec2(a.x(), b.y()) / (tamMalla - 1)).r()*altura_total);
	}
	else {
		c = osg::Vec3(b.x(), a.y(), imagen_heightmap->getColor(osg::Vec2(b.x(), a.y()) / (tamMalla - 1)).r()*altura_total);;
	}
	double d;
	//plano ax+by+cz+d=0
	//vn=(a,b,c)
	vn = (c - a) ^ (b - a);
	d = -(vn.x()*a.x() + vn.y()*a.y() + vn.z()*a.z());
	//printf("%f|||%f|||%f\n", posX, posY, -(d + vn.x()*xGeom + vn.y()*yGeom) / vn.z());
	
	return -(d + vn.x()*xGeom + vn.y()*yGeom) / vn.z();
	
}


osg::Geode* CreaMalla(int num_puntos,double separacion_puntos)
{
	osg::Geometry* geom = new osg::Geometry;
	osg::Geode* geode = new osg::Geode;
	geode->addDrawable(geom);
	
	osg::Vec3Array* v = new osg::Vec3Array;
	osg::Vec2Array* texcoords1 = new osg::Vec2Array(); //Vectores de coordenadas
	osg::Vec2Array* texcoords2 = new osg::Vec2Array();
	for( int y = -num_puntos/2; y < num_puntos/2; y++)
	{
		for( int x = -num_puntos/2; x < num_puntos/2; x++)
			{
				double a = double(x*separacion_puntos);
				double b =  double(y*separacion_puntos);
				v->push_back(osg::Vec3(a,b,0)); //Push_back es como la funci�n que a�ad�a al final de una lista
				texcoords1->push_back(osg::Vec2(((float)(x+(num_puntos/2))/(num_puntos-1)),((float)(y+(num_puntos/2)))/(num_puntos-1))); //Algoritmo para traducir a coordenadas
				texcoords2->push_back(osg::Vec2(((float)(x+(num_puntos/2))/(num_puntos-1)),((float)(y+(num_puntos/2)))/(num_puntos-1)));
			}
		}
	


	geom->setVertexArray(v);
	geom->setTexCoordArray(0,texcoords1);
	geom->setTexCoordArray(1,texcoords2);


	osg::DrawElementsUInt* elements = new osg::DrawElementsUInt(osg::PrimitiveSet::QUADS);
	for(unsigned int y=0;y<num_puntos-1;y++)
	{
		for(unsigned int x=0;x<num_puntos-1;x++)
		{
			elements->push_back((y+1)*(num_puntos)+x);
			elements->push_back(y*(num_puntos)+x);
			elements->push_back(y*(num_puntos)+x+1);
			elements->push_back((y+1)*(num_puntos)+x+1);
		}
	}
	
	
	geom->addPrimitiveSet(elements);
	return geode;
}

void AddTexture(osg::Node* node, std::string nombre_imagen, int unidad_textura)
{
	const char *vinit[] = {"texUnit0","texUnit1","texUnit2","texUnit3","texUnit4","texUnit5","texUnit6","texUnit7"};
	osg::Uniform*	t = new osg::Uniform(vinit[unidad_textura],unidad_textura); //Creaci�n de uniforms, cuidado de no pisar estos al hacer uno manual


	node->getOrCreateStateSet()->addUniform(t);

	
	
	osg::Image* image = osgDB::readImageFile( nombre_imagen);	//Lectura imagen
	osg::Texture2D* textura = new osg::Texture2D(image);
	textura->setFilter(osg::Texture2D::MIN_FILTER,osg::Texture2D::LINEAR_MIPMAP_LINEAR);
	textura->setFilter(osg::Texture2D::MAG_FILTER,osg::Texture2D::LINEAR);
	textura->setWrap(osg::Texture2D::WRAP_S,osg::Texture2D::REPEAT); //Esto era CLAMP, temas de bordes, repasar llegado el momento, creo que por eso vuelve a subir fuera del heightmap
	textura->setWrap(osg::Texture2D::WRAP_T,osg::Texture2D::REPEAT);

	
	node->getOrCreateStateSet()->setTextureAttributeAndModes(unidad_textura,textura,osg::StateAttribute::ON | osg::StateAttribute::OVERRIDE | osg::StateAttribute::PROTECTED);
	
	osg::AlphaFunc* alphaFunc = new osg::AlphaFunc;
    alphaFunc->setFunction(osg::AlphaFunc::GEQUAL,0.05f);
	node->getOrCreateStateSet()->setAttributeAndModes( alphaFunc, osg::StateAttribute::ON );

}

void AddShader(osg::Node* node,std::string vertex_shader,std::string fragment_shader)

{
	osg::Program* ProgramObject = new osg::Program;

	osg::Shader* VertexObject =   new osg::Shader( osg::Shader::VERTEX );
	VertexObject->loadShaderSourceFromFile(vertex_shader);

	osg::Shader* FragmentObject = new osg::Shader( osg::Shader::FRAGMENT);
	FragmentObject->loadShaderSourceFromFile(fragment_shader);

	ProgramObject->addShader(FragmentObject) && ProgramObject->addShader(VertexObject);

	node->getOrCreateStateSet()->setAttributeAndModes(ProgramObject, osg::StateAttribute::ON | osg::StateAttribute::PROTECTED);

}

double inc_tiempo()
{
	/*
	double intervalo = std::chrono::duration_cast<std::chrono::milliseconds>
		(t_fin - t_ini).count();
	return intervalo;
	*/
	static auto tiempo_anterior = std::chrono::system_clock::now();

	auto tiempo = std::chrono::system_clock::now();

	double incremento = std::chrono::duration_cast<std::chrono::milliseconds>(tiempo - tiempo_anterior).count();

	tiempo_anterior = tiempo;

	return incremento;
}