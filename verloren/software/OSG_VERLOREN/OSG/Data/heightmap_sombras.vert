uniform sampler2D texUnit0;	
uniform sampler2D texUnit1;


varying float height;
varying vec3 normal;
void main(void)
{
	vec4 color = texture2D(texUnit0, gl_MultiTexCoord0.st);
	normal = vec3(texture2D(texUnit1, gl_MultiTexCoord0.st));
	normal = 2*normal - vec3(1.0,1.0,1.0);
	normal = gl_NormalMatrix * normal;
	vec4 vertice = gl_Vertex;
	height =color.r*100.0;
	
	vertice[2] = height;
	
	gl_TexCoord[0] = gl_MultiTexCoord0; 
	gl_Position = gl_ModelViewProjectionMatrix * vertice;
}
