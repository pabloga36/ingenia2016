#pragma once

#include "objeto.h"

class objetoMovil :
	public objeto
{
public:
	objetoMovil(double a, double b, double esc, osg::Group * grupo, osg::Node * geometria,double ost);
	void movimiento( osgViewer::Viewer* viewer);
	void objetoMovil::setVX(double v);
	void objetoMovil::setVY(double v);
	void objetoMovil::setW(double v);
	void objetoMovil::setW2(double v);
	~objetoMovil();
protected:
	float vx;
	float vy;
	float w;
	float w2;
	std::chrono::time_point<std::chrono::steady_clock>  t;
};

