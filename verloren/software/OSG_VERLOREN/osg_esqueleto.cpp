﻿
#include "stdafx.h"

#include "FuncionesAyuda.h"
#include "config_file.h"
#include "objeto.h"
#include "objetoMovil.h"
#include "texto_HUD.h"
#include "Image_HUD.h"
#include "inventario.h"

//Variables globales


int width, height;
double posx, posy;
int H = 0.5;
int L = 0.5;
int t = ::GetCurrentTime();
struct Imagen hambre;
osg::Image* imagen_heightmap = osgDB::readImageFile(HEIGHTMAP);
osg::Geometry* imagen = new osg::Geometry;
osg::Camera* camera_texto = new osg::Camera;
osg::Camera* camara2 = new osg::Camera;
osg::Camera* camara3 = new osg::Camera;
osg::ref_ptr<osgText::Font> font = osgText::readRefFontFile("fonts/arial.ttf");
double vx, vy, radio=5.0,wrot,w2,x_cam,y_cam,z_cam;
std::string recurso = "";
bool mostrando=false,coger=false;

class SimulacionKeyboardEvent : public osgGA::GUIEventHandler
{
protected:
public:
	SimulacionKeyboardEvent::SimulacionKeyboardEvent()
	{
	};
	struct control_teclado
	{
		double x;
		double y;

	} tecla_pulsada;

	virtual bool SimulacionKeyboardEvent::handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter&)
	{
		int tecla = ea.getKey();
		int evento = ea.getEventType();
		int shift = ea.getModKeyMask();
		int button = ea.getButton();
		double x = ea.getX();
		double y = ea.getY();

		switch (evento)
		{
		case(osgGA::GUIEventAdapter::MOVE):
		{
			wrot = SENSIBILIDAD*(posx-x)/(radio*1000);
			w2 = SENSIBILIDAD*(posy-y) / (radio * 10000);
			posx = x;
			posy = y;
			
		}
		
		case(osgGA::GUIEventAdapter::KEYDOWN):	//Evento pulsacion tecla
		{
			switch (tecla) {
				case'a':
					vx = VELOCIDAD;
					break;
				case'd':
					vx = -VELOCIDAD;
					break;
				case'w':
					vy = -VELOCIDAD;
					break;
				case's':
					vy = VELOCIDAD;
					break;
				case'j':
					wrot = 0.001;
					break;
				case'l':
					wrot = -0.001;
					break;
				case'i':
					w2 = 0.001;
					break;
				case'k':
					w2 = -0.001;
					break;
				case'e':
					coger=true;
					break;
				case 'g':
					if (H > 0)
					{
						H -= 0.5;
						t = ::GetCurrentTime();
						Image_HUD::modify(0.5, 7.5, 20 - H, 12, hambre.g);

					}
				default:
					break;
			}
			break;
		}
		//case (osgGA::GUIEventAdapter::PUSH):
		case(osgGA::GUIEventAdapter::KEYUP):	// Se deja de pulsar una tecla
		{
			switch (tecla) {
			case'a':
				vx = 0;
				break;
			case'd':
				vx = 0;
				break;
			case'w':
				vy = 0;
				break;
			case's':
				vy = 0;
				break;
			case'j':
				wrot = 0;
				break;
			case'l':
				wrot = 0;
				break;
			case'i':
				w2 = 0;
				break;
			case'k':
				w2 = 0;
				break;
			case'r':
				mostrando = !mostrando;
				break;
			default:
				break;
			}
			break;
		}
		default:
			break;
		}
		return 0;
		
	}
	};

	
	int main(int, char**)
	{
		
		osgViewer::Viewer viewer;
	
		viewer.setUpViewInWindow(50, 50, 800, 600, 0);
		unsigned int w, h;
		osg::GraphicsContext::WindowingSystemInterface* wsi =
			osg::GraphicsContext::getWindowingSystemInterface();
		if (!wsi)
		{
				return 0;
		}
		wsi->getScreenResolution(osg::GraphicsContext::ScreenIdentifier(0),
			w, h);
		width = w;
		height = h;

		osg::Group * grupo = new osg::Group;
		viewer.addEventHandler(new SimulacionKeyboardEvent());
		osg::Geode* malla = CreaMalla(MALLA, SEPARACION);											// Crea la malla del suelo
		AddTexture(malla, HEIGHTMAP, 0);		// Añade la textura a la malla
		AddTexture(malla, "..\\Data\\Texturas\\default.png", 1);		// Añade la textura a la malla
		AddTexture(malla, "..\\Data\\Texturas\\default_d.png", 2);		// Añade la textura a la malla
		AddTexture(malla, "..\\Data\\Texturas\\orangesand.png", 3);		// Añade la textura a la malla
		AddTexture(malla, "..\\Data\\Texturas\\longGrass.png", 4);		// Añade la textura a la malla
		AddTexture(malla, "..\\Data\\Texturas\\packDirt.png", 5);		// Añade la textura a la malla
		AddTexture(malla, "..\\Data\\Texturas\\ocean_texture.png", 7);
		AddShader(malla, "..\\Data\\heightmap_sombras.vert", "..\\Data\\heightmap_sombras.frag");
		grupo->addChild(malla);															//Añade malla al grupo
																						//----------------------------- Luces ----------------------------------

		osg::Light* myLight = new osg::Light;
		myLight->setLightNum(0);
		myLight->setPosition(osg::Vec4(1000.0, 0.0, 0.0, 0.0f));				// Posicion foco
		myLight->setAmbient(osg::Vec4(1.0f, 1.0f, 1.0f, 1.0f));				// Color luz ambiente
		myLight->setDiffuse(osg::Vec4(0.5f, 0.5f, 0.45f, 1.0f));				// Color luz difusa

		osg::LightSource* lightS = new osg::LightSource;
		lightS->setLight(myLight);
		lightS->setLocalStateSetModes(osg::StateAttribute::ON);

		osg::Group * grupo_luz = new osg::Group;
		grupo_luz->addChild(lightS);
		grupo->addChild(grupo_luz);


		//------------------------------ Sombras ------------------------------------

		osgShadow::ShadowedScene* sc = new  osgShadow::ShadowedScene;
		osg::ref_ptr<osgShadow::LightSpacePerspectiveShadowMapVB> sm = new osgShadow::LightSpacePerspectiveShadowMapVB;

		sc->setShadowTechnique(sm);

		float minLightMargin = 10.f;
		float maxFarPlane = 1000;
		unsigned int texSize = 1024;
		unsigned int baseTexUnit = 0;
		unsigned int shadowTexUnit = 6;

		texSize = 4096;
		sm->setLight(myLight);
		sm->setMinLightMargin(minLightMargin);
		sm->setMaxFarPlane(maxFarPlane);
		sm->setTextureSize(osg::Vec2s(texSize, texSize));
		sm->setShadowTextureCoordIndex(shadowTexUnit);
		sm->setShadowTextureUnit(shadowTexUnit);
		sm->setBaseTextureCoordIndex(baseTexUnit);
		sm->setBaseTextureUnit(baseTexUnit);

		const int ReceivesShadowTraversalMask = 0x1;
		const int CastsShadowTraversalMask = 0x2;

		sc->setReceivesShadowTraversalMask(ReceivesShadowTraversalMask);
		sc->setCastsShadowTraversalMask(CastsShadowTraversalMask);

		grupo->addChild(sc);

		viewer.setSceneData(grupo);



		//--------------------------- Camara_texto ------------------------------- 
		//osg::Camera* camera_texto = new osg::Camera;
		camera_texto->setProjectionMatrix(osg::Matrix::ortho2D(-width / 2.0, width / 2.0, -height / 2.0, height / 2.0));
		camera_texto->setReferenceFrame(osg::Transform::ABSOLUTE_RF);
		camera_texto->setViewMatrix(osg::Matrix::identity());
		camera_texto->setClearMask(GL_DEPTH_BUFFER_BIT);
		camera_texto->setRenderOrder(osg::Camera::POST_RENDER,0);
		grupo->addChild(camera_texto);
		camara2->setProjectionMatrix(osg::Matrix::ortho2D(-width / 2.0, width / 2.0, -height / 2.0, height / 2.0));
		camara2->setReferenceFrame(osg::Transform::ABSOLUTE_RF);
		camara2->setViewMatrix(osg::Matrix::identity());
		camara2->setClearMask(GL_DEPTH_BUFFER_BIT);
		camara2->setRenderOrder(osg::Camera::POST_RENDER, 1);
		grupo->addChild(camara2);
		camara3->setProjectionMatrix(osg::Matrix::ortho2D(-width / 2.0, width / 2.0, -height / 2.0, height / 2.0));
		camara3->setReferenceFrame(osg::Transform::ABSOLUTE_RF);
		camara3->setViewMatrix(osg::Matrix::identity());
		camara3->setClearMask(GL_DEPTH_BUFFER_BIT);
		camara3->setRenderOrder(osg::Camera::POST_RENDER, 4);
		grupo->addChild(camara3);

		//----------------------------- Textos e Imagenes ----------------------------
		//Textos
		struct Texto titulo = texto_HUD::add(0, 97, "Lost Paradise v_alfa_0.5");
		struct Texto vida_texto = texto_HUD::add(7, 5.2, "VIDA");
		struct Texto hambre_texto = texto_HUD::add(6, 12.2, "HAMBRE");

		// Barra de hambre

		hambre = Image_HUD::add(0.5, 7.5, 20, 12, 0, "..//Data//Texturas//HUD//color-amarillo.png");
		// Barra de vida
		struct Imagen vida = Image_HUD::add(0.5, 0.5, 20, 5, 0, "..//Data//Texturas//HUD//barra_de_vida.png");
		struct Imagen vida_rojo = Image_HUD::add(0.5, 0.5, 20, 5, 0, "..//Data//Texturas//HUD//Heightmap_rgb.png");

		struct Imagen fondo = Image_HUD::add(0.5, 0.5, 22, 14, 0, "..//Data//Texturas//HUD//gris.jpg");
		//-------------Geometrias------------------
			


		osg::MatrixTransform * mt = NULL;
		osg::Matrixd mtras, mrotz, mrotx, mroty, mesc, mtotal;
		std::map <std::string, osg::Node *> geometrias;
		std::map <std::string, std::string> recursos;
		int i = 0;
		osg::Billboard* BB = NULL;
		float escala = 1.0, xpos = 0.0, ypos = 0.0, zpos = 0.0;
		geometrias["personaje"] = osgDB::readNodeFile(".\\modelos\\Sam_Peters\\Sam_Peters.obj");
		geometrias["tanque"] = osgDB::readNodeFile(".\\modelos\\Arbol\\bush.OSGB");
		using namespace rapidxml;
		config_file config("./layout.xml");
		std::string fichero;
		xml_document<> doc;    // character type defaults to char
		doc.parse<0>(config.buffer);    // 0 means default parse flags
		auto node_datos = doc.first_node();
		for (xml_node<> *node = node_datos->first_node(); node != NULL; node = node->next_sibling())
		{
			mt = new osg::MatrixTransform;
			for (xml_node<> *nodo = node->first_node(); nodo != NULL; nodo = nodo->next_sibling())
			{
				if (strcmp(nodo->name(), "Fichero") == 0) {
					fichero = nodo->value();
					if (geometrias.find(fichero) == geometrias.end())
					{
						geometrias[fichero] = osgDB::readNodeFile(nodo->value());
					}
					mt->addChild(geometrias[fichero]);
				}
				if (strcmp(nodo->name(), "Recurso") == 0) {
					if (recursos.find(fichero) == recursos.end())
					{
						fichero=fichero.substr(fichero.find_last_of("\\")+1);
						fichero=fichero.substr(0, fichero.find_last_of("."));
						printf("%s", fichero.c_str());
						recursos[fichero] = nodo->value();
					}
				}

				if (strcmp(nodo->name(), "Escala") == 0) {
					escala = atof(nodo->value());

				}

				if (strcmp(nodo->name(), "Offset") == 0) {
					zpos = atof(nodo->value());
				}
				if (strcmp(nodo->name(), "Xpos") == 0) {
					xpos = atof(nodo->value());

				}

				if (strcmp(nodo->name(), "Ypos") == 0) {
					ypos = atof(nodo->value());

				}

			}
			zpos += obtenerAltura(xpos, ypos, MALLA, SEPARACION, H_MAX, imagen_heightmap);
			if(zpos>24.45 && abs(xpos)<255 && abs(ypos)<255){
				mesc = mesc.scale(escala, escala, escala);
				mtras = mtras.translate(xpos, ypos, zpos);
				mtotal = mesc*mtras;
				mt->setMatrix(mtotal);
				grupo->addChild(mt);
			}
		}
		
		//BB = new osg::Billboard;
		//BB->setMode(osg::Billboard::AXIAL_ROT);
		//BB->setAxis(osg::Vec3(0, 0, 1));
		//BB->setNormal(osg::Vec3(0, -1, 0));
		//BB->addDrawable(geometrias["vrl_serg.3DS"]->asGeode()->getDrawable(0));
		//mt->addChild(BB);
		double offset = -0.5;
		objetoMovil personaje(0, 0,0.35, grupo, geometrias["personaje"],offset);
		//Iniciacion colision
		osg::ref_ptr<osgUtil::LineSegmentIntersector> intersector =
			new osgUtil::LineSegmentIntersector(osg::Vec3(0, 0, 0), osg::Vec3(1, 1, 1));
		osgUtil::IntersectionVisitor iv(intersector.get());
		viewer.getCamera()->accept(iv);
		osgUtil::LineSegmentIntersector::Intersection firstHit =
			intersector->getFirstIntersection();
		inventario inv(3, 4);
		viewer.realize();
		while (!viewer.done()) {
			if (mostrando) {
				inv.mostrar();
			}
			else {
				inv.desactivar();
			}
			personaje.setVX(vx);
			personaje.setVY(vy);
			personaje.setW(wrot);
			personaje.setW2(w2);
			wrot = 0;
			w2 = 0;
			personaje.movimiento(&viewer);
			x_cam = personaje.X() + radio* cos(personaje.AlphaG() - PI_ / 2)*sin(personaje.ThetaG());
			y_cam = personaje.Y() + radio* sin(personaje.AlphaG() - PI_ / 2)*sin(personaje.ThetaG());
			z_cam = personaje.Z() + radio*cos(personaje.ThetaG()) + 1.0;
			if (z_cam < obtenerAltura(x_cam, y_cam, MALLA, SEPARACION, H_MAX, imagen_heightmap) + 0.2) {
				z_cam = obtenerAltura(x_cam, y_cam, MALLA, SEPARACION, H_MAX, imagen_heightmap) + 0.2;
			}
			viewer.getCamera()->setViewMatrixAsLookAt(osg::Vec3d(x_cam, y_cam, z_cam), osg::Vec3d(personaje.X(), personaje.Y(), personaje.Z() + 1.0), osg::Vec3d(0, 0, 1));
			//viewer.getCamera()->setViewMatrixAsLookAt(osg::Vec3d(0, 0, 100), osg::Vec3d(personaje.X(),personaje.Y(), 0), osg::Vec3d(1, 0, 0));
			// First intersection:
			intersector->setStart(osg::Vec3d(personaje.X(), personaje.Y(), personaje.Z() + 0.3));
			intersector->setEnd((osg::Vec3d(personaje.X() - 1.5*cos(personaje.AlphaG() + PI_ / 2.0), personaje.Y() - 1.5*sin(personaje.AlphaG() + PI_ / 2.0), personaje.Z() + 0.3)));
			iv.setIntersector(intersector.get());
			viewer.getCamera()->accept(iv);
			firstHit = intersector->getFirstIntersection();
			for (i = 0; i < firstHit.nodePath.size(); i++) {
				//printf("%s", firstHit.nodePath[i]->getName().c_str());
				if (recursos.find(firstHit.nodePath[i]->getName()) != recursos.end()) {
					recurso = recursos[firstHit.nodePath[i]->getName()];
					if (coger) {
						inv.añadir(recurso, 1, 20);
					}
					break;
				}
			}
			coger = false;
			intersector->reset();
			if ((::GetCurrentTime() - t) >= 1000 && H < 20)
			{
				Image_HUD::modify(0.5, 7.5, 20 - H, 12, hambre.g);
				H = H++;
				t = ::GetCurrentTime();
				if ( H <= 4 && L>0) {
					L = L--;
					Image_HUD::modify(0.5, 0.5, 20 - L, 5, vida.g);
				}
			}
			if ((::GetCurrentTime() - t) >= 1000 && H >= 19.5 && L < 20)

			{
				L = L++;
				Image_HUD::modify(0.5, 0.5, 20 - L, 5, vida.g);
				
				t = ::GetCurrentTime();
			}

			
			viewer.frame();
			Sleep(33);

		}
		return 0;

	}
