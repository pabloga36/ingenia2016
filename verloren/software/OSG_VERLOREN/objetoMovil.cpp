
#include "stdafx.h"
#include "osg\LineSegment"
#include "osgUtil\IntersectVisitor"	
#include "objetoMovil.h"
#include "FuncionesAyuda.h"
extern osg::Image *imagen_heightmap;
objetoMovil::objetoMovil(double a, double b, double esc, osg::Group * grupo, osg::Node * geometria,double ost)
	:objeto(a, b, esc, grupo, geometria,ost)
{
	vx = 0.0;
	vy = 0.0;
	w = 0.0;
	w2 = 0.0;
	// usar chrono
	// t= ::GetCurrentTime();
}

void objetoMovil::setVX(double v) {
	vx = v;
}

void objetoMovil::setVY(double v) {
	vy = v;
}

void objetoMovil::setW(double v) {
	w = v;
}

void objetoMovil::setW2(double v) {
	w2 = v;
}

void objetoMovil::movimiento(osgViewer::Viewer* viewer) {

		//Declaracion de varaiables necesarias
		osg::Matrix mtras, mrotz, mesc;
		double x_ant = x;
		double y_ant = y;
		double z_ant = z;
		double intervalo = inc_tiempo();
		double x_post = x +vx*intervalo*cos(alpha*180.0 / PI_) - vy*intervalo*sin(alpha*180.0 / PI_);
		double y_post = y + vx*intervalo*sin(alpha*180.0 / PI_) + vy*intervalo*cos(alpha*180.0 / PI_);
		double z_post = obtenerAltura(x_post, y_post, MALLA, SEPARACION, H_MAX, imagen_heightmap);
		alpha = alpha + w *intervalo / 100.0;
		theta = theta + w2*intervalo / 100.0;
		
		
		mrotz = mrotz.rotate(alpha*180.0 / PI_, osg::Vec3d(0, 0, 1));
		mesc = mesc.scale(escala, escala, escala);
		if ((abs(x_post)<255) && abs(y_post<255) && (vx != 0 || vy != 0) && (z_post - z_ant) / sqrt((x_post - x_ant)*(x_post - x_ant) + (y_post - y_ant)*(y_post - y_ant)) < 1) {
			printf("%f///%f\n", x_ant, x_post);
			//osgUtil::LineSegmentIntersector personLocationSegment(osg::Vec3(x_ant, y_ant, z_ant), osg::Vec3(x_post, y_post, z_post));
			//osgUtil::LineSegmentIntersector::Intersection interseccion =personLocationSegment.getFirstIntersection();
			osg::ref_ptr<osgUtil::LineSegmentIntersector> intersector =
				new osgUtil::LineSegmentIntersector(osg::Vec3(x_ant, y_ant, z_ant+0.5), osg::Vec3(2*x_post-x_ant, 2*y_post-y_ant, z_post+0.5));
			osgUtil::IntersectionVisitor iv(intersector.get());
			viewer->getCamera()->accept(iv);

			// First intersection: 
			osgUtil::LineSegmentIntersector::Intersection firstHit =
				intersector->getFirstIntersection();
			printf("%d\n", intersector->getIntersections().size());
			
			//Asignar los puntos iniciales del rayo
			//Devolver las nuevas coordenadas resultado de la interseccion
			if (intersector->getIntersections().size()>2 || (intersector->getIntersections().size() > 0 && intersector->getIntersections().size() <= 2 && firstHit.nodePath[4]->getName().find("Sam_legs")))
			{
				
					x = x_ant;
					y = y_ant;
					z = z_ant;
					mtras = mtras.translate(x_ant, y_ant, z_ant);

				
			}
			else {

				x = x_post;
				y = y_post;
				z = z_post;
				mtras = mtras.translate(x_post, y_post, z_post);
				

			}

		}
		else {
			mtras = mtras.translate(x, y, z);
		}

		mt->setMatrix(mrotz*mesc*mtras);

	}




objetoMovil::~objetoMovil()
{
}
