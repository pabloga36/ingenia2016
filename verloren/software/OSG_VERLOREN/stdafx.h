// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once
#define MALLA 512
#define SEPARACION 1
#define HEIGHTMAP "..\\Data\\Texturas\\default.png"
#define H_MAX 100.0
#define VELOCIDAD 0.01
#define PI_ 3.14159265358979323846
#define SENSIBILIDAD 5

#include <iostream>
#include <tchar.h>
#include <WinSock2.h>

#include <string>
#include <chrono>
#include <math.h>
#include <iostream>
#include <vector>
#include <array>
#include <iterator>
#include <fstream>

#include "rapidxml.hpp"
#include "rapidxml_iterators.hpp"
#include "rapidxml_print.hpp"
#include "rapidxml_utils.hpp"

#include <osg/Geode>
#include <osg/Geometry>
#include <osg/AlphaFunc>
#include <osg/Texture2D>
#include <osgDB/readFile>

#include <osg/AutoTransform>
#include <osg/Billboard>
#include <osg/BlendFunc>
#include <osg/Depth>
#include <osg/Geode>
#include <osg/Geometry>
#include <osg/Group>
#include <osg/Light>
#include <osg/LightSource>
#include <osg/LineWidth>
#include <osg/Material>
#include <osg/MatrixTransform>
#include <osg/Node>
#include <osg/Notify>
#include <osg/PositionAttitudeTransform>
#include <osg/Projection>
#include <osg/ShapeDrawable>
#include <osg/StateSet>
#include <osg/TexEnv>
#include <osg/TexGen>
#include <osg/TexMat>
#include <osg/Texture2D>
#include <osgText/Text>

#include <osgDB/ReadFile>
#include <osgDB/Registry>
#include <osgDB/WriteFile>

#include <osgGA/TrackballManipulator>

#include <osgShadow/ShadowedScene>
#include <osgShadow/ShadowVolume>
#include <osgShadow/ShadowTexture>
#include <osgShadow/ShadowMap>
#include <osgShadow/SoftShadowMap>
#include <osgShadow/ParallelSplitShadowMap>
#include <osgShadow/LightSpacePerspectiveShadowMap>
#include <osgShadow/StandardShadowMap>
#include <osgShadow/ViewDependentShadowMap>

#include <osgViewer/Viewer>
//Clases

// TODO: reference additional headers your program requires here
