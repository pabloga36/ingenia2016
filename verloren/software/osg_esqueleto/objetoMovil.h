#pragma once
#include <winsock2.h>
#include "objeto.h"
#include "stdafx.h"

class objetoMovil :
	public objeto
{
public:
	objetoMovil(double a, double b, double esc, osg::Group * grupo, osg::Node * geometria,double ost);
	void movimiento(double tiempo);
	void objetoMovil::setVX(double v);
	void objetoMovil::setVY(double v);
	void objetoMovil::setW(double v);
	~objetoMovil();
protected:
	float vx;
	float vy;
	float w;
	double t;
};

