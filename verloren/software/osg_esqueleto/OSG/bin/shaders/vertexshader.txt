uniform TexCoord[], texUnit0;
varying float z;
void main(){
gl_TexCoord[0] = gl_MultiTexCoord;
vec4 altura = texture2D(texUnit0,gl_TexCoord[0].st);
vec4 posicion=gl_Vertex;
posicion.z=altura.r * 1550.0;
z= posicion.z;
gl_Position = gl_ModelViewProjectionMatrix *posicion.z;
}