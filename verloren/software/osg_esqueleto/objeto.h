#pragma once
#include "stdafx.h"
class objeto
{
public:
	objeto(double a, double b, double esc, osg::Group * grupo, osg::Node * geometria,double ost);
	double X();
	double Y();
	double Z();
	double Alpha();
	double AlphaG();
	~objeto();
protected:
	double x;
	double y;
	double z;
	double alpha;
	double escala;
	double offset;
	osg::MatrixTransform * mt;

};