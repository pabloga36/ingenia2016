﻿#pragma once

#define WIN32_LEAN_AND_MEAN    // stops windows.h including winsock.h

#include <winsock2.h>

#include "stdafx.h"
#include "FuncionesAyuda.h"
#include "config_file.h"
#include "objeto.h"
#include "objetoMovil.h"

double vx, vy, w;
osg::Image* imagen_heightmap = osgDB::readImageFile(HEIGHTMAP);

class SimulacionKeyboardEvent : public osgGA::GUIEventHandler
{
protected:
public:
	SimulacionKeyboardEvent::SimulacionKeyboardEvent()
	{
	};
	struct control_teclado
	{
		double x;
		double y;

	} tecla_pulsada;

	virtual bool SimulacionKeyboardEvent::handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter&)
	{
		int tecla = ea.getKey();
		int evento = ea.getEventType();
		int shift = ea.getModKeyMask();
		int button = ea.getButton();

		switch (evento)
		{
		case(osgGA::GUIEventAdapter::KEYDOWN):	//Evento pulsacion tecla
		{
			switch (tecla) {
				case'a':
					vx = VELOCIDAD;
					break;
				case'd':
					vx = -VELOCIDAD;
					break;
				case'w':
					vy = -VELOCIDAD;
					break;
				case's':
					vy = VELOCIDAD;
					break;
				case'j':
					w = 0.001;
					break;
				case'k':
					w = -0.001;
					break;
				default:
					break;
			}
			break;
		}
		//case (osgGA::GUIEventAdapter::PUSH):
		case(osgGA::GUIEventAdapter::KEYUP):	// Se deja de pulsar una tecla
		{
			switch (tecla) {
			case'a':
				vx = 0;
				
				break;
			case'd':
				vx = 0;
				break;
			case'w':
				vy = 0;
				break;
			case's':
				vy = 0;
				break;
			case'j':
				w = 0;
				break;
			case'k':
				w = 0;
				break;
			default:
				break;
			}
			break;
		}
		default:
			break;
		}
		return 0;
		
	}
	};


	int main(int, char**)
	{

		osgViewer::Viewer viewer;
		viewer.setUpViewInWindow(50, 50, 800, 600, 0);
		osg::Group * grupo = new osg::Group;
		viewer.addEventHandler(new SimulacionKeyboardEvent());
		osg::Geode* malla = CreaMalla(MALLA, SEPARACION);											// Crea la malla del suelo
		AddTexture(malla, HEIGHTMAP, 0);		// Añade la textura a la malla
		AddTexture(malla, "..\\Data\\Texturas\\krakatoa.png", 1);		// Añade la textura a la malla
		AddTexture(malla, "..\\Data\\Texturas\\krakatoa275_rgb.png", 2);		// Añade la textura a la malla
		AddTexture(malla, "..\\Data\\Texturas\\cesped.dds", 3);		// Añade la textura a la malla
		AddTexture(malla, "..\\Data\\Texturas\\snow.dds", 4);		// Añade la textura a la malla
		AddTexture(malla, "..\\Data\\Texturas\\1-ocean.dds", 5);		// Añade la textura a la malla
		AddShader(malla, "..\\Data\\heightmap_sombras.vert", "..\\Data\\heightmap_sombras.frag");
		grupo->addChild(malla);															//Añade malla al grupo
																						//----------------------------- Luces ----------------------------------

		osg::Light* myLight = new osg::Light;
		myLight->setLightNum(0);
		myLight->setPosition(osg::Vec4(1000.0, 0.0, 0.0, 0.0f));				// Posicion foco
		myLight->setAmbient(osg::Vec4(1.0f, 1.0f, 1.0f, 1.0f));				// Color luz ambiente
		myLight->setDiffuse(osg::Vec4(0.5f, 0.5f, 0.45f, 1.0f));				// Color luz difusa

		osg::LightSource* lightS = new osg::LightSource;
		lightS->setLight(myLight);
		lightS->setLocalStateSetModes(osg::StateAttribute::ON);

		osg::Group * grupo_luz = new osg::Group;
		grupo_luz->addChild(lightS);
		grupo->addChild(grupo_luz);


		//------------------------------ Sombras ------------------------------------

		osgShadow::ShadowedScene* sc = new  osgShadow::ShadowedScene;
		osg::ref_ptr<osgShadow::LightSpacePerspectiveShadowMapVB> sm = new osgShadow::LightSpacePerspectiveShadowMapVB;

		sc->setShadowTechnique(sm);

		float minLightMargin = 10.f;
		float maxFarPlane = 1000;
		unsigned int texSize = 1024;
		unsigned int baseTexUnit = 0;
		unsigned int shadowTexUnit = 6;

		texSize = 4096;
		sm->setLight(myLight);
		sm->setMinLightMargin(minLightMargin);
		sm->setMaxFarPlane(maxFarPlane);
		sm->setTextureSize(osg::Vec2s(texSize, texSize));
		sm->setShadowTextureCoordIndex(shadowTexUnit);
		sm->setShadowTextureUnit(shadowTexUnit);
		sm->setBaseTextureCoordIndex(baseTexUnit);
		sm->setBaseTextureUnit(baseTexUnit);

		const int ReceivesShadowTraversalMask = 0x1;
		const int CastsShadowTraversalMask = 0x2;

		sc->setReceivesShadowTraversalMask(ReceivesShadowTraversalMask);
		sc->setCastsShadowTraversalMask(CastsShadowTraversalMask);

		grupo->addChild(sc);

		viewer.setSceneData(grupo);


		//--------------------------- Camara_texto ------------------------------- 
		osg::Camera* camera_texto = new osg::Camera;
		camera_texto->setProjectionMatrix(osg::Matrix::ortho2D(-400, 400, -300, 300));
		camera_texto->setReferenceFrame(osg::Transform::ABSOLUTE_RF);
		camera_texto->setViewMatrix(osg::Matrix::identity());
		camera_texto->setClearMask(GL_DEPTH_BUFFER_BIT);
		camera_texto->setRenderOrder(osg::Camera::POST_RENDER);
		grupo->addChild(camera_texto);

		//----------------------------- Texto ----------------------------
		osg::ref_ptr<osgText::Font> font = osgText::readRefFontFile("fonts/arial.ttf");
		osg::Geode* geode_texto_intro = new osg::Geode();
		osgText::Text* texto_intro = new osgText::Text;
		geode_texto_intro->addDrawable(texto_intro);
		camera_texto->addChild(geode_texto_intro);

		texto_intro->setFont(font);
		texto_intro->setPosition(osg::Vec3(0.0f, 0.0f, 0.0f));
		texto_intro->setFontResolution(40, 40);
		texto_intro->setCharacterSizeMode(osgText::Text::SCREEN_COORDS);
		texto_intro->setCharacterSize(20);
		texto_intro->setAlignment(osgText::Text::LEFT_BOTTOM);
		texto_intro->setText("");
		//-------------Geometrias------------------



		osg::MatrixTransform * mt = NULL;
		osg::Matrixd mtras, mrotz, mrotx, mroty, mesc, mtotal;
		std::map <std::string, osg::Node *> geometrias;
		int i = 0;
		osg::Billboard* BB = NULL;
		float escala = 1.0, xpos = 0.0, ypos = 0.0;
		geometrias["personaje"] = osgDB::readNodeFile(".\\modelos\\trip\\trip.obj");
		/*using namespace rapidxml;
		config_file config("./Arbol_layout.xml");

		xml_document<> doc;    // character type defaults to char
		doc.parse<0>(config.buffer);    // 0 means default parse flags
		auto node_datos = doc.first_node();
		for (xml_node<> *node = node_datos->first_node(); node != NULL; node = node->next_sibling())
		{
			mt = new osg::MatrixTransform;
			for (xml_node<> *nodo = node->first_node(); nodo != NULL; nodo = nodo->next_sibling())
			{
				if (strcmp(nodo->name(), "Fichero") == 0) {
					if (geometrias.find(nodo->value()) == geometrias.end())
					{
						geometrias[nodo->value()] = osgDB::readNodeFile(nodo->value());
					}
					mt->addChild(geometrias[nodo->value()]);
				}
				if (strcmp(nodo->name(), "Escala") == 0) {
					escala = atof(nodo->value());

				}

				if (strcmp(nodo->name(), "Xpos") == 0) {
					xpos = atof(nodo->value());

				}

				if (strcmp(nodo->name(), "Ypos") == 0) {
					ypos = atof(nodo->value());

				}

			}
			mesc = mesc.scale(escala, escala, escala);
			mtras = mtras.translate(xpos, ypos, 0);
			mtotal = mesc*mtras;
			mt->setMatrix(mtotal);
			grupo->addChild(mt);
		}*/


		//BB = new osg::Billboard;
		//BB->setMode(osg::Billboard::AXIAL_ROT);
		//BB->setAxis(osg::Vec3(0, 0, 1));
		//BB->setNormal(osg::Vec3(0, -1, 0));
		//BB->addDrawable(geometrias["vrl_serg.3DS"]->asGeode()->getDrawable(0));
		//mt->addChild(BB);
		double offset = -0.005;
		objetoMovil personaje(0, 0,0.005, grupo, geometrias["personaje"],offset);

		double radio = 5.0;
		viewer.realize();
		while (!viewer.done()) {
			personaje.setVX(vx);
			personaje.setVY(vy);
			personaje.setW(w);
			personaje.movimiento(::GetCurrentTime());
			viewer.getCamera()->setViewMatrixAsLookAt(osg::Vec3d(personaje.X() + radio* cos(personaje.AlphaG()+PI/2.0),personaje.Y() + radio* sin(personaje.AlphaG() + PI / 2.0),personaje.Z() + 1.0), osg::Vec3d(personaje.X(), personaje.Y(),personaje.Z() + 1.0), osg::Vec3d(0, 0, 1));
			//viewer.getCamera()->setViewMatrixAsLookAt(osg::Vec3d(0, 0, 100), osg::Vec3d(personaje.X(),personaje.Y(), 0), osg::Vec3d(1, 0, 0));
			viewer.frame();
			Sleep(33);

		}
		return 0;

	}
