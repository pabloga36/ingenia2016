#include "objetoMovil.h"


extern osg::Image *imagen_heightmap;
objetoMovil::objetoMovil(double a, double b, double esc, osg::Group * grupo, osg::Node * geometria,double ost)
	:objeto(a, b, esc, grupo, geometria,ost)
{
	vx = 0.0;
	vy = 0.0;
	w = 0.0;
	t= ::GetCurrentTime();
}
void objetoMovil::setVX(double v) {
	vx = v;
}
void objetoMovil::setVY(double v) {
	vy = v;
}

void objetoMovil::setW(double v) {
	w = v;
}

void objetoMovil::movimiento(double tiempo) {
	osg::Matrix mtras, mrotz,mesc;
	x = x + vx*(tiempo - t)*cos(alpha*180.0 / PI) - vy*(tiempo - t)*sin(alpha*180.0 / PI);
	y = y + vx*(tiempo - t)*sin(alpha*180.0 / PI) + vy*(tiempo - t)*cos(alpha*180.0 / PI);
	alpha = alpha + w / 100.0*(tiempo - t);
	mrotz = mrotz.rotate(alpha*180.0 / PI, osg::Vec3d(0,0,1));
	mesc = mesc.scale(escala, escala, escala);
	z = obtenerAltura(x, y, MALLA, SEPARACION, H_MAX, imagen_heightmap) + offset;
	mtras = mtras.translate(x, y, z);
	mt->setMatrix(mrotz*mesc*mtras);
	t = ::GetCurrentTime();
}


objetoMovil::~objetoMovil()
{
}
