#include "objeto.h"
extern osg::Image *imagen_heightmap;
objeto::objeto(double a, double b, double esc, osg::Group * grupo, osg::Node * geometria, double ost)
	:x(a), y(b), escala(esc), offset(ost)
{
	osg::Matrix mtras, mrotz;
	osg::Matrixd mesc;
	mt = new osg::MatrixTransform;
	alpha = 0;
	mrotz = mrotz.rotate(alpha*180.0 / PI, osg::Vec3d(0, 0, 1));
	mesc = mesc.scale(esc, esc, esc);
	z = obtenerAltura(a, b, MALLA, SEPARACION, H_MAX, imagen_heightmap) + offset;
	mtras = mtras.translate(a, b, z);
	mt->setMatrix(mrotz*mesc*mtras);
	mt->addChild(geometria);
	grupo->addChild(mt);	
	
}
double objeto::X() {
	return x;
};
double objeto::Y() {
	return y;
};
double objeto::Z() {
	return z;
};
double objeto::Alpha() {
	return alpha;
};
double objeto::AlphaG() {
	return alpha*180/PI;
};

objeto::~objeto()
{
}
